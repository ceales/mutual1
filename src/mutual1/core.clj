(ns mutual1.core
  (:require [criterium.core :refer [bench]]))

;; An implementation of mutual exclusion.
;;
;; The two fields p0 and p1 are both functions that take
;; a function of work to be done and return a new function that is wrapped in an
;; implementation of mututal exclusion so that if (p0 f1) and (p1 f2) are
;; executed concurrently then f1 and f2 should not execute concurrently.
(defrecord two-process-exclusion-impl
    [p0 p1])

(def non-exclusive-pair
  (->two-process-exclusion-impl identity identity))

(defn two-flag-exclusive-pair
  [two-flag-impl]
  (let [flag0 (atom false)
        flag1 (atom false)]
    (->two-process-exclusion-impl
     (partial two-flag-impl flag0 flag1)
     (partial two-flag-impl flag1 flag0))))

(defn exclusion-1
  [my-flag other-flag f]
  (fn []
    (while @other-flag #_DoNothing)
    (reset! my-flag true)
    (f)
    (reset! my-flag false)))

(def exclusion-1-pair
  (two-flag-exclusive-pair exclusion-1))

(defn exclusion-2
  [my-flag other-flag f]
  (fn []
    (reset! my-flag true)
    (while @other-flag #_DoNothing)
    (f)
    (reset! my-flag false)))

(def exclusion-2-pair
  (two-flag-exclusive-pair exclusion-2))

(defn exclusion-3
  [my-flag other-flag f]
  (fn []
    (reset! my-flag true)
    (while @other-flag
      (reset! my-flag false)
      (reset! my-flag true))
    (f)
    (reset! my-flag false)))

(def exclusion-3-pair
  (two-flag-exclusive-pair exclusion-3))

(defn exclusion-4
  [my-flag other-flag i-yield f]
  (fn []
    (reset! my-flag true)
    (while @other-flag
      (when i-yield
        (reset! my-flag false)
        (while @other-flag #_DoNothing)
        (reset! my-flag true)))
    (f)
    (reset! my-flag false)))

(defn set-yield-value
  [two-flag-pair]
  (->two-process-exclusion-impl
   (partial (:p0 two-flag-pair) false)
   (partial (:p1 two-flag-pair) true)))

(def exclusion-4-pair
  (-> exclusion-4
      two-flag-exclusive-pair
      set-yield-value))

(defn exclusion-5
  [my-flag other-flag turn-flag my-turn f]
  (fn []
    (reset! my-flag true)
    (while @other-flag
      (when (not= @turn-flag my-turn)
        (reset! my-flag false)
        (while @other-flag #_DoNothing)
        (reset! my-flag true)))
    (f)
    (reset! turn-flag (not my-turn))
    (reset! my-flag false)))

(defn add-turn-flag
  [two-flag-pair]
  (let [turn-flag (atom false)]
    (->two-process-exclusion-impl
     (partial (:p0 two-flag-pair) turn-flag false)
     (partial (:p1 two-flag-pair) turn-flag true))))

(def exclusion-5-pair
  (-> exclusion-5
      two-flag-exclusive-pair
      add-turn-flag))

(defn exclusion-6
  [my-flag other-flag turn-flag my-turn f]
  (fn []
    (reset! my-flag true)
    (while @other-flag
      (when (not= @turn-flag my-turn)
        (reset! my-flag false)
        (while (not= @turn-flag my-turn) #_DoNothing)
        (reset! my-flag true)))
    (f)
    (reset! turn-flag (not my-turn))
    (reset! my-flag false)))

(def exclusion-6-pair
  (-> exclusion-6
      two-flag-exclusive-pair
      add-turn-flag))

(defn test-exclusion
  [runs impl-pair]
  (let [x (volatile! 0)
        f (fn [] (vreset! x (inc @x)))
        p0 ((:p0 impl-pair) f)
        p1 ((:p1 impl-pair) f)
        t0 (future (dotimes [_ runs] (p0)))
        t1 (future (dotimes [_ runs] (p1)))]
    [(deref t0 2000 "Timeout")
     (deref t1 2000 "Timeout")
     @x]))

