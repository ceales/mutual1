(defproject mutual1 "0.1.0-SNAPSHOT"
  :description "Just the code examples to support my Dekker's Algorithm blog post"
  :url "https://cloud.eales-toms.com/blog/mutual-exclusion-dekkers-algorithm/"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [criterium "0.4.4"]])
