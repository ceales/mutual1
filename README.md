# mutual1 - Dekker's Algorithm in Clojure

The code examples to support my blog post on Dekker's Algorithm with
implementation in Clojure.

https://cloud.eales-toms.com/blog/mutual-exclusion-dekkers-algorithm/

## License

Copyright © 2016 Craig Eales

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
